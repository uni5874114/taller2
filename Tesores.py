import tensorflow as tf
import time
tamanio = 10000

matriz_A = tf.random.uniform(shape=(tamanio, tamanio))
matriz_B = tf.random.uniform(shape=(tamanio, tamanio))

matriz_A = tf.cast(matriz_A, dtype=tf.float32)
matriz_B = tf.cast(matriz_B, dtype=tf.float32)

inicio = time.time()

resultado = tf.matmul(matriz_A, matriz_B)

fin = time.time()

tiempo_transcurrido = fin - inicio

print(f"Tiempo transcurrido: {tamanio}x{tamanio}: {tiempo_transcurrido} segundos")