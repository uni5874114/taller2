import numpy as np
import time
tamanio = 10000

matriz_A = np.random.rand(tamanio, tamanio)
matriz_B = np.random.rand(tamanio, tamanio)

inicio = time.time()

resultado = np.dot(matriz_A, matriz_B)

fin = time.time()

tiempo_transcurrido = fin - inicio

print(f"Tiempo transcurrido: {tamanio}x{tamanio}: {tiempo_transcurrido} segundos")