import tensorflow as tf
import time

# Configurar el entorno para usar TPU
resolver = tf.distribute.cluster_resolver.TPUClusterResolver(tpu='')
tf.config.experimental_connect_to_cluster(resolver)
tf.tpu.experimental.initialize_tpu_system(resolver)
strategy = tf.distribute.TPUStrategy(resolver)

# Definir el tamaño de la matriz
tamanio = 10000

# Generar matrices aleatorias
with strategy.scope():  # Esta línea garantiza que las operaciones se realicen en la TPU
    matriz_A = tf.random.uniform(shape=(tamanio, tamanio))
    matriz_B = tf.random.uniform(shape=(tamanio, tamanio))

    matriz_A = tf.cast(matriz_A, dtype=tf.float32)
    matriz_B = tf.cast(matriz_B, dtype=tf.float32)

# Medir el tiempo de multiplicación de matrices
inicio = time.time()
with strategy.scope():  # Esta línea garantiza que las operaciones se realicen en la TPU
    resultado = tf.matmul(matriz_A, matriz_B)
fin = time.time()

tiempo_transcurrido = fin - inicio

print(f"Tiempo transcurrido: {tamanio}x{tamanio}: {tiempo_transcurrido} segundos")